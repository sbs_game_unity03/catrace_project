using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class CatInstance : MonoBehaviour
{
	[Header("# 고양이 타입")]
	public CatType m_CatType;

	/// <summary>
	/// 끝 지점을 나타냅니다.
	/// </summary>
	public Transform m_FinishTransform;

	/// <summary>
	/// 초기 위치를 나타냅니다.
	/// </summary>
	private Vector3 _InitialPosition;

	/// <summary>
	/// 고양이 속력을 나타냅니다.
	/// </summary>
	private float _Speed;

	/// <summary>
	/// 애니메이션을 재생하기 위한 Animator 컴포넌트입니다.
	/// </summary>
	private Animator _Animator;

	/// <summary>
	/// 이동 방향을 나타냅니다.
	/// </summary>
	private Vector3 _Direction;

	// 이동 허용 상태입니다.
	private bool _AllowMove;

	public event System.Action<CatType> onRaceFinished;


	private void Awake()
	{
		_InitialPosition = transform.position;

		_Animator = GetComponent<Animator>();

		// 방향 구하기
		_Direction = (m_FinishTransform.position - transform.position).normalized;
	}

	private void Update()
	{
		// 이동
		Move();
	}

	private void Move()
	{
		if (!_AllowMove) return;

		float speed = _Speed * Time.deltaTime;
        float remainDistance = Vector3.Distance(transform.position, m_FinishTransform.position);


		// 마지막 이동인지 확인합니다.
		bool isLastMove = (remainDistance <= speed);

		// 마지막 이동인 경우 속력을 남은 거리로 설정합니다.
		if (isLastMove)
		{
			speed = remainDistance;
			_AllowMove = false;

			// 레이스 끝 이벤트 발생
			onRaceFinished?.Invoke(m_CatType);
		}

		transform.position += speed * _Direction;
	}

	/// <summary>
	/// 위치를 초기화합니다.
	/// </summary>
	public void InitPosition()
	{
		transform.position = _InitialPosition;
	}

	/// <summary>
	/// 이동을 시작합니다.
	/// </summary>
	public void StartMove()
	{
		_Speed = Random.Range(2.0f, 3.0f);
		_AllowMove = true;
		_Animator.SetBool("_Move", true);
	}

	public void FinishMove()
	{
		_AllowMove = false;
		_Animator.SetBool("_Move", false);
	}

}
