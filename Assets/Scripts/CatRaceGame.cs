using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;



public class CatRaceGame : MonoBehaviour
{
	[Header("# 고양이 선택 패널")]
	public  RectTransform m_SelectPanel;

	[Header("# 결과 텍스트")]
	public TMP_Text m_ResultText;

	[Header("# 결과 텍스트")]
	public Button m_RestartButton;

	/// <summary>
	/// 고양이 객체들을 나타냅니다.
	/// </summary>
	public List<CatInstance> m_CatInstances;


	/// <summary>
	/// 선택한 고양이 타입
	/// </summary>
	private CatType _SelectedCatType;



	private void Awake()
	{
		m_ResultText.gameObject.SetActive(false);
		m_RestartButton.gameObject.SetActive(false);

		m_RestartButton.onClick.AddListener(InitGame);

		foreach (CatInstance catInstance in m_CatInstances)
			catInstance.onRaceFinished += OnFinishRace;
	}

	/// <summary>
	/// 선택된 고양이를 설정합니다.
	/// </summary>
	/// <param name="selectedCat"></param>
	public void SetSelectedCat(CatType selectedCat)
	{
		_SelectedCatType = selectedCat;
	}

	/// <summary>
	/// 게임을 초기화합니다.
	/// </summary>
	public void InitGame()
	{

		m_ResultText.gameObject.SetActive(false);
		m_RestartButton.gameObject.SetActive(false);
		m_SelectPanel.gameObject.SetActive(true);

		// 모든 고양이의 위치를 초기화합니다.
		foreach (CatInstance catInstance in m_CatInstances)
			catInstance.InitPosition();
	}

	/// <summary>
	/// 게임을 시작합니다.
	/// </summary>
	public void StartGame()
	{
		m_SelectPanel.gameObject.SetActive(false);

		// 모든 고양이의 이동을 시작합니다.
		foreach (CatInstance catInstance in m_CatInstances)
			catInstance.StartMove();
	}

	public void OnFinishRace(CatType catType)
	{
		m_ResultText.text = catType.ToString();
		m_ResultText.gameObject.SetActive(true);
		m_RestartButton.gameObject.SetActive(true);

		// 모든 고양이의 이동을 끝냅니다.
		foreach (CatInstance catInstance in m_CatInstances)
			catInstance.FinishMove();
	}


}
