using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 고양이 선택 버튼
/// </summary>
public class CatSelectButton : MonoBehaviour
{
	[Header("# 고양이 타입")]
	public CatType m_CatType;

	/// <summary>
	/// 고양이 경주 게임 객체
	/// </summary>
	private CatRaceGame _CatRaceGame;

	/// <summary>
	/// 버튼 컴포넌트
	/// </summary>
	private Button _Button;

	private void Awake()
	{
		// 이 컴포넌트와 같은 위치에서 Button 컴포넌트를 찾습니다.
		_Button = GetComponent<Button>();

		// 월드에서 CatRaceGame과 일치하는 컴포넌트 객체를 찾습니다.
		_CatRaceGame = GameObject.FindObjectOfType<CatRaceGame>();

		_Button.onClick.AddListener(OnButtonClicked);
	}

	private void OnButtonClicked()
	{
		// 고양이 선택
		_CatRaceGame.SetSelectedCat(m_CatType);

		// 경주를 시작시킵니다.
		_CatRaceGame.StartGame();
	}


}
